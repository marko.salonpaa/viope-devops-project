# DevOps Example App

This is a simple web app that uses Python and Flask.

## Installation

Install Python 3.9. Then run `pip install -r requirements.txt` to install the dependencies.

## Running the app

Run the app in development with

```
FLASK_APP=app.py python -m flask run
```

Now you can access the application by opening http://127.0.0.1:5000/ with your browser!